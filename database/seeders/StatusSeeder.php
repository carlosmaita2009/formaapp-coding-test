<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'id' => 1,
            'name' => 'visible'
        ]);
        DB::table('statuses')->insert([
            'id' => 2,
            'name' => 'Oculto'
        ]);
    }
}

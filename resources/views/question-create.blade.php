@extends('layouts.main')

@section('content')
    <div class="container-create">
        <div class="form-create">
        <h1 class="title-create">Crear Pregunta</h1>
            <form action="{{route('question.store')}}" method="POST">
                @csrf
                @method('POST')
                <div class="field-status">
                    <p class="text-label">Estado</p>
                    <select class="select-status" name="status_id" id="status">
                        @foreach ($status as $s)
                            <option value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field-content">
                    <p class="text-label">Enunciado</p>
                    <textarea name="content" id="content" cols="30" rows="10"></textarea>
                </div>
                <div class="space-button">
                    <input class="button-form" type="submit" value="Crear">
                </div>
                <div class="messages">
                    @error('content')
                        <div class="alert-danger">{{ $message }}</div>
                    @enderror
                    @if(session('message'))
                        <div class="alert-success">{{session('message')}}</div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection

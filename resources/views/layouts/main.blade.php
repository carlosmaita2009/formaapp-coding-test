<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{env('APP_NAME')}}</title>
        <link href="/css/app.css" rel="stylesheet">
        <script src="/js/app.js" ></script>
    </head>
    <body>
        <nav class="header">
            <div class="logo">
                <img class="img-logo" src="https://play-lh.googleusercontent.com/wuPg0CmDKkTGbXdR16fV_BGwA5kLqK53UqDOZeQp9Evc66Bjsm4z-yHIlRHKad308TY" alt="Logo">
            </div>
            <div class="nav-item">
                <a href="{{route('question.list')}}">
                    Lista de preguntas
                </a>
            </div>
            <div class="nav-item">
                <a href="{{route('question.create')}}">
                    Crear preguntas
                </a>
            </div>
        </nav>
        <main class="container">
            @yield('content')
        </main>
        @yield('modals')
    </body>
</html>

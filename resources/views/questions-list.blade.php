@extends('layouts.main')

@section('content')
    <div class="container-list">
        <h1 class="title-list">Listado de preguntas</h1>
        <div class="list">
            <div class="head-list">
                <div class="item-title">Id</div>
                <div class="item-title span-2">Enunciado</div>
                <div class="item-title">Estado</div>
                <div class="item-title">Acción</div>
            </div>
            {{-- Valores de la tabla --}}
            @foreach ($questions as $q)
            <div class="row-list" id="question-{{$q->id}}">
                <div class="item-value">{{$q->id}}</div>
                <div class="item-value span-2">{{$q->content}}</div>
                <div class="item-value">{{$q->status->name}}</div>
                <div class="item-value">
                    <button class="btn-delete" data-id="{{$q->id}}"><img src="{{asset('icons/trash.svg')}}" alt="eliminar">Eliminar</button>
                </div>
            </div>
            @endforeach

        </div>
    </div>
@endsection

@section('modals')
<!-- The Modal -->
<div id="delete-modal" class="modal">
    <div class="modal-content">
      <input id="modal-question_id" type="hidden" name="question_id">
      <div class="modal-question">
        <p>
            ¿Estás seguro de que quieres borrar la pregunta?
        </p>
        </div>
      <div class="modal-buttons">
         <button id="btn-yes"  class="button-yes">Si</button>
         <button id="btn-no" class="button-no">No</button>
      </div>
    </div>
</div>
@endsection

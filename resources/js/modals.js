window.onload = () =>{
    // Get the modal
    const $modal = document.getElementById("delete-modal");
    const $btnNo = document.getElementById("btn-no");
    const $btnYes = document.getElementById("btn-yes");
    const $btns = document.querySelectorAll(".btn-delete");
    
    if($modal){
        // When the user clicks on the button, open the modal
        if($btns){
            $btns.forEach((btn)=>{
                btn.onclick = function() {
                   $modal.style.display = "block";
                   $btnYes.dataset.id = btn.dataset.id;
                }
            })
        }
        
        // When the user clicks on NO, close the modal
        if($btnNo){
            $btnNo.onclick = function(e) {
                $modal.style.display = "none";
            }
        }

        // When the user clicks on NO, close the modal
        if($btnYes){
            $btnYes.onclick = function(e) {
                removeQuestion($btnYes.dataset.id);
                $modal.style.display = "none";
            }
        }
        async function removeQuestion(id){
            try{
                let url =`/api/questions/${id}`
                let response = await fetch( url , {
                    method: "DELETE"
                })
                let data = await response.json()
                // console.log(data)
                if (data.status == "OK" ){
                    document.getElementById(`question-${id}`).remove();
                }
            }catch(e){
                console.error(e); 
            }
        }
        
        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target == $modal) {
            $modal.style.display = "none";
          }
        } 
    }
}
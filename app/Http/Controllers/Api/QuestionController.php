<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function showQuestionVisible(){
        $response =  array(
            "status" => "OK",
            "data" => array(
                "questions" => Question::allvisible()
            )
        );
        return response($response ,200);
    }

    public function destroy($id){
        $id = Question::destroy($id);
        if($id !== 0 ){
            $response = array(
                "status" => "OK",
                "data" => array(
                    "question_id" => $id
                )
            );
        }else{
            $response = array(
                "status" => "KO",
                "data" => array(
                    "error_message" => "Something when wrong"
                )
            );  
        }
        return response()->json($response, 200) ;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Models\Question;
use App\Models\Status;

class QuestionController extends Controller
{
    public function index (){
        $questions = Question::all();
        return view('questions-list', compact('questions'));
    }

    public function create(){
        $status = Status::all();
        return view('question-create', compact('status'));
    }

    public function store(QuestionRequest $request){
        $validated = $request->validated();
        $q = Question::create($request->all());
        return redirect()->route('question.create' )
            ->with('message', 'La pregunta ha sido creada con exito');
    }

}
